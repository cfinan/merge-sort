# Getting started with external-merge-sort

__version__: `0.1.1dev0`

[This](https://gitlab.com/cfinan/external-merge-sort) is a pure Python external merge sort implementation. It is orders of magnitude slower then the UNIX sort but if you need to do an external merge sort and do not want to make a system call to UNIX sort then this should help you out. It uses the excellent `heapq.merge` under the hood.

A full external merge sort, is implemented in two separate steps. The initial subsetting and sorting of the input file into temp files, then the merging of those temp files and the iteration of the sorted rows. The merge allows you to control how many temp files are open at one time. I wrote this as I could not find many options for doing this in Python. If anyone knows of anything else please contact me and I will add it here as a possible alternative to this package.

There is [online](https://cfinan.gitlab.io/external-merge-sort/index.html) documentation for external-merge-sort and offline PDF documentation can be downloaded [here](https://gitlab.com/cfinan/external-merge-sort/-/blob/master/resources/pdf/external-merge-sort.pdf).

## Installation instructions
At present, external-merge-sort is undergoing development and no packages exist yet on PyPi. Therefore it is recommended that it is installed in either of the two ways listed below. First, clone this repository and then `cd` to the root of the repository.

```
git clone git@gitlab.com:cfinan/external-merge-sort.git
cd external-merge-sort
```

### Installation not using any conda dependencies
If you are not using conda in any way then install the dependencies via `pip` and install external-merge-sort as an editable install also via pip:

Install dependencies:
```
python -m pip install --upgrade -r requirements.txt
```

For an editable (developer) install run the command below from the root of the external-merge-sort repository (or drop `-e` for static install):
```
python -m pip install -e .
```

### Installation using conda
A conda build is also available for Python v3.7, v3.8 and v3.9 on linux-64.
```
conda install -c cfin external-merge-sort
```

If you are using conda and require anything different then please install with pip and install the dependencies with the environments specified in `resources/conda_env`. There is also a build recipe in `./resourses/build/conda` that can be used to create new packages.

## Basic usage
There are some examples in `./resources/examples` where `.` is the root of the external-merge-sort repository.

## Run tests
If you have cloned the repository, you can also run the tests using `pytest ./tests`, if any fail please contact us (see the contribution page for contact info).
