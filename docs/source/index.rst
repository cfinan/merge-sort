Welcome to merge-sort
=====================

`This <https://gitlab.com/cfinan/merge-sort>`_ is a pure Python external merge sort implementation. It is orders of magnitude slower then the GNU sort but if you need to do an external merge sort and do not want to make a system call to GNU sort then this should help you out. It uses the excellent ``heapq.merge`` under the hood.

A full external merge sort, is implemented in two separate steps. The initial subsetting and sorting of the input file into temp files, then the merging of those temp files and the iteration of the sorted rows. The merge allows you to control how many temp files are open at one time. I wrote this as I could not find many options for doing this in Python. If anyone knows of anything else please contact me and I will add it here as a possible alternative to this package.

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   getting_started

.. toctree::
   :maxdepth: 2
   :caption: Programmer reference

   usage
   api

.. toctree::
   :maxdepth: 2
   :caption: Project admin

   contributing


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
