# Getting started with merge-sort

__version__: `0.3.1a0`

[This](https://gitlab.com/cfinan/merge-sort) is a pure Python external merge sort implementation. It is orders of magnitude slower then the UNIX sort but if you need to do an external merge sort and do not want to make a system call to UNIX sort then this should help you out. It uses the excellent `heapq.merge` under the hood.

A full external merge sort, is implemented in two separate steps. The initial subsetting and sorting of the input file into temp files, then the merging of those temp files and the iteration of the sorted rows. The merge allows you to control how many temp files are open at one time. I wrote this as I could not find many options for doing this in Python. If anyone knows of anything else please contact me and I will add it here as a possible alternative to this package.

There is [online](https://cfinan.gitlab.io/merge-sort/index.html) documentation for merge-sort.

## Installation instructions
At present, merge-sort is undergoing development and no packages exist yet on PyPi. Therefore it is recommended that it is installed in either of the two ways listed below.

### Installation using pip
You can install using pip from the root of the cloned repository, first clone and cd into the repository root:

```
git clone git@gitlab.com:cfinan/merge-sort.git
cd merge-sort
```

Install the dependencies:
```
python -m pip install --upgrade -r requirements.txt
```

Then install using pip
```
python -m pip install .
```

Or for an editable (developer) install run the command below from the root of the merge-sort repository. The difference with this is that you can just to a `git pull` to update merge_sort, or switch branches without re-installing:
```
python -m pip install -e .
```

### Installation using conda
I maintain a conda package in my personal conda channel. To install this please run:

```
conda install -c cfin -c conda-forge merge-sort
```

### Conda dependencies
There are also conda yaml environment files in `./resources/conda/env` that have the same contents as `requirements.txt` but for conda packages, so all the pre-requisites. I use this to install all the requirements via conda and then install the package as an editable pip install.

However, if you find these useful then please use them. There are Conda environments for Python v3.8, v3.9 and v3.10.

## Basic usage
There are some examples in `./resources/examples` where `.` is the root of the merge-sort repository.

## Run tests
If you have cloned the repository, you can also run the tests using `pytest ./tests`, if any fail please contact us (see the contribution page for contact info).

## Change log
Below are some brief notes of changes implemented in different versions.

### `v0.3.0a0`
* Added option for skipping comment lines that occur before the header line/first data line.

### `v0.3.1a0`
* Requirements and conda build fixes.
