"""Tests for chunking data rows into sorted chunks using different methods.
This tests ``merge_sort.chunks`` modules.
"""
from merge_sort import chunks, examples
import pytest
import os
import math
import csv
import common
# import pprint as pp


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def check_chunk_files(files, data, data_types, test_key, chunksize,
                      delimiter, open_method, has_header=True, **csv_kwargs):
    """Check that the chunk files are of the expected structure when compared
    to the source data.

    Parameters
    ----------
    files : `list` of `str`
        The chunk files in the order the were created by the chunker. i.e.
        ``chunker.chunk_file_list``.
    data : `list` of `list`
        The source data to compare against.
    data_types : `list` of `str`
        The data types for the columns in ``data``. These are used to convert
        the data that is read in from a chunk file, so it can be compared to
        the source ``data``.
    test_key : `function`
        A sort key function that will be applied to slices of ``data``, should
        be the same as that used in the chunker except for not requiring any
        delimiter split.
    chunksize : `int`
        The chunk size used in the chunker.
    delimiter : `str`
        The delimiter used to join the rows prior to adding to the chunker.
    open_method : `function`
        The method used by the chunker to write to chunk files.
    has_header : `bool`, optional, default: `True`
        Were the chunk files written with a header value.
    **csv_kwargs
        If the chunker was a csv chunker then place any csv keyword arguments
        that should be applied to the test chunk files here (with the exception
        of the delimiter).
    """
    use_csv = False
    use_csv_dict = False
    if len(csv_kwargs) > 0:
        use_csv = True
        if isinstance(data[1], dict):
            use_csv = False
            use_csv_dict = True

    start_idx = 0
    if has_header is True:
        start_idx = 1

    for f, i in zip(files, range(start_idx, len(data)+1, chunksize)):
        chunk_file = []
        with open_method(f, 'rt') as infile:
            use_file = infile
            if has_header is True and use_csv_dict is False:
                next(infile)

            if use_csv is True:
                use_file = csv.reader(infile, delimiter=delimiter,
                                      **csv_kwargs)
            elif use_csv_dict is True:
                use_file = csv.DictReader(infile, delimiter=delimiter,
                                          **csv_kwargs)
            for row in use_file:
                if use_csv is True:
                    row = [examples._DTYPE_MAP[d](v)
                           for d, v in zip(data_types, row)]
                elif use_csv_dict is True:
                    row = dict([
                        (k, examples._DTYPE_MAP[d](v))
                        for d, k, v in zip(
                                data_types.values(), row.keys(), row.values()
                        )
                    ])
                else:
                    row = [examples._DTYPE_MAP[d](v)
                           for d, v in zip(data_types, row.split(delimiter))]
                chunk_file.append(row)

        data_chunk = sorted(data[i:i + chunksize], key=test_key)
        assert chunk_file == data_chunk, "wrong chunk values"


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@pytest.mark.parametrize(
    ("has_header,ncols,nrows,chunksize,key_dtypes,delimiter,chunk_suffix,"
     "write_method,use_class"),
    [
        (True, 10, 10, 3, (str, str), "\t", ".chunk", open,
         chunks.SortedChunks),
        (True, 10, 10, 5, (str, str), "\t", ".chunk", open,
         chunks.SortedChunks),
        (True, 10, 10, 20, (str, str), "\t", ".chunk", open,
         chunks.SortedChunks),
        (True, 10, 10, 1, (str, str), "\t", ".chunk", open,
         chunks.SortedChunks),
        (True, 10, 10, 3, (str, int), "\t", ".chunk", open,
         chunks.SortedChunks),
        (True, 10, 10, 5, (str, int), "\t", ".chunk", open,
         chunks.SortedChunks),
        (True, 10, 10, 20, (str, int), "\t", ".chunk", open,
         chunks.SortedChunks),
        (True, 10, 10, 1, (str, int), "\t", ".chunk", open,
         chunks.SortedChunks),
        (True, 10, 10, 3, (str, float), "\t", ".chunk", open,
         chunks.SortedChunks),
        (True, 10, 10, 5, (str, float), "\t", ".chunk", open,
         chunks.SortedChunks),
        (True, 10, 10, 20, (str, float), "\t", ".chunk", open,
         chunks.SortedChunks),
        (True, 10, 10, 1, (str, float), "\t", ".chunk", open,
         chunks.SortedChunks),
        (True, 10, 10, 3, (int, int), "\t", ".chunk", open,
         chunks.SortedChunks),
        (True, 10, 10, 5, (int, int), "\t", ".chunk", open,
         chunks.SortedChunks),
        (True, 10, 10, 20, (int, int), "\t", ".chunk", open,
         chunks.SortedChunks),
        (True, 10, 10, 1, (int, int), "\t", ".chunk", open,
         chunks.SortedChunks),
        (True, 10, 10, 3, (int, float), "\t", ".chunk", open,
         chunks.SortedChunks),
        (True, 10, 10, 5, (int, float), "\t", ".chunk", open,
         chunks.SortedChunks),
        (True, 10, 10, 20, (int, float), "\t", ".chunk", open,
         chunks.SortedChunks),
        (True, 10, 10, 1, (int, float), "\t", ".chunk", open,
         chunks.SortedChunks),
        (True, 10, 10, 3, (float, float), "\t", ".chunk", open,
         chunks.SortedChunks),
        (True, 10, 10, 5, (float, float), "\t", ".chunk", open,
         chunks.SortedChunks),
        (True, 10, 10, 20, (float, float), "\t", ".chunk", open,
         chunks.SortedChunks),
        (True, 10, 10, 1, (float, float), "\t", ".chunk", open,
         chunks.SortedChunks),
        (True, 10, 10, 3, (str, str), "\t", ".chunk", open,
         chunks.SortedListChunks),
        (True, 10, 10, 5, (str, str), "\t", ".chunk", open,
         chunks.SortedListChunks),
        (True, 10, 10, 20, (str, str), "\t", ".chunk", open,
         chunks.SortedListChunks),
        (True, 10, 10, 1, (str, str), "\t", ".chunk", open,
         chunks.SortedListChunks),
        (True, 10, 10, 3, (str, int), "\t", ".chunk", open,
         chunks.SortedListChunks),
        (True, 10, 10, 5, (str, int), "\t", ".chunk", open,
         chunks.SortedListChunks),
        (True, 10, 10, 20, (str, int), "\t", ".chunk", open,
         chunks.SortedListChunks),
        (True, 10, 10, 1, (str, int), "\t", ".chunk", open,
         chunks.SortedListChunks),
        (True, 10, 10, 3, (str, float), "\t", ".chunk", open,
         chunks.SortedListChunks),
        (True, 10, 10, 5, (str, float), "\t", ".chunk", open,
         chunks.SortedListChunks),
        (True, 10, 10, 20, (str, float), "\t", ".chunk", open,
         chunks.SortedListChunks),
        (True, 10, 10, 1, (str, float), "\t", ".chunk", open,
         chunks.SortedListChunks),
        (True, 10, 10, 3, (int, int), "\t", ".chunk", open,
         chunks.SortedListChunks),
        (True, 10, 10, 5, (int, int), "\t", ".chunk", open,
         chunks.SortedListChunks),
        (True, 10, 10, 20, (int, int), "\t", ".chunk", open,
         chunks.SortedListChunks),
        (True, 10, 10, 1, (int, int), "\t", ".chunk", open,
         chunks.SortedListChunks),
        (True, 10, 10, 3, (int, float), "\t", ".chunk", open,
         chunks.SortedListChunks),
        (True, 10, 10, 5, (int, float), "\t", ".chunk", open,
         chunks.SortedListChunks),
        (True, 10, 10, 20, (int, float), "\t", ".chunk", open,
         chunks.SortedListChunks),
        (True, 10, 10, 1, (int, float), "\t", ".chunk", open,
         chunks.SortedListChunks),
        (True, 10, 10, 3, (float, float), "\t", ".chunk", open,
         chunks.SortedListChunks),
        (True, 10, 10, 5, (float, float), "\t", ".chunk", open,
         chunks.SortedListChunks),
        (True, 10, 10, 20, (float, float), "\t", ".chunk", open,
         chunks.SortedListChunks),
        (True, 10, 10, 1, (float, float), "\t", ".chunk", open,
         chunks.SortedListChunks),
        (False, 10, 10, 3, (str, str), "\t", ".chunk", open,
         chunks.SortedChunks),
        (False, 10, 10, 5, (str, str), "\t", ".chunk", open,
         chunks.SortedChunks),
        (False, 10, 10, 20, (str, str), "\t", ".chunk", open,
         chunks.SortedChunks),
        (False, 10, 10, 1, (str, str), "\t", ".chunk", open,
         chunks.SortedChunks),
    ]
)
def test_sorted_chunks(tmpdir, has_header, ncols, nrows, chunksize, key_dtypes,
                       delimiter, chunk_suffix, write_method, use_class):
    """Tests for the ``merge_sort.chunks.SortedChunks`` and
    ``merge_sort.chunks.SortedListChunks``. This applies the chunkers to some
    test data and ensures the expected chunk files are created.
    """
    # Get test data and data types that are in each column
    data, data_types = examples.create_data(has_header=has_header, ncols=ncols,
                                          nrows=nrows)
    # Get the sort key that will be given to the chunker
    sort_key, key_cols = common.get_list_sort_func(data_types, key_dtypes,
                                                   delimiter=delimiter)
    # Get the sort key that will be used on the test data (does not require
    # delimiter splitting in the sort key)
    test_key, key_cols = common.get_list_sort_func(data_types, key_dtypes,
                                                   delimiter=None)

    # These conditions must be met for the test to be valid
    assert data == data, "invalid test"

    start_idx = 0
    header = None
    if has_header is True:
        start_idx = 1
        header = delimiter.join(data[0]) + "\n"

    # Initialise the chunker and add the rows
    chunker = use_class(
        tmpdir, key=sort_key, chunksize=chunksize,
        header=header, chunk_suffix=chunk_suffix,
        write_method=write_method
    )

    with chunker as c:
        for row in data[start_idx:]:
            c.add_row(
                "{0}\n".format(delimiter.join([str(i) for i in row]))
            )

    # glob the directory and get the chunk files that have been output
    glob_files = common.get_chunk_files(tmpdir, suffix=chunk_suffix)

    # Make sure we have the expected number of rows
    assert len(glob_files) == math.ceil(
        (len(data) - int(has_header)) / chunksize
    ), "wrong file number"

    # Make sure we have output all the files that the chunker thinks it has
    # output
    chunk_files = c.chunk_file_list
    assert sorted(chunk_files) == sorted(glob_files), "wrong files"

    # Now check that all the files have the expected content
    check_chunk_files(chunk_files, data, data_types, test_key, chunksize,
                      delimiter, write_method, has_header=has_header)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def set_switches(data, sort_key, chunksize, switches, has_header=True):
    """Prepare the data for the test and get expected file sizes.

    Parameters
    ----------
    data : `list` of `list`
        The source data to sort and switch.
    sort_key : `function`
        The sort key function to apply to the ``data`` sort.
    chunksize : `int`
        The chunk size used in the chunker.
    switches : `list` of `int`
        The 0-based start index positions where the switch will occur.
    has_header : `bool`, optional, default: `True`
        Were the chunk files written with a header value.

    Returns
    -------
    data : `list` of `list`
        The source data with switches. Not really necessary to return as the
        switches happen in place.
    file_sizes : `list` of `int`
        The number of expected data rows in the output files returned by the
        chunker. The length of the list is the number of files.

    Notes
    -----
    This sorts the data into the correct order and then switches specific
    adjacent rows to force the extended chunker to create a new file.
    """
    # If we have a header then remove the first row for the switch routine
    header = []
    if has_header is True:
        header = data.pop(0)

    # Sort the data before performing row switches
    data.sort(key=sort_key)

    file_sizes = []
    for i in switches:
        if len(data) == i+1:
            raise IndexError("can't switch last element")

        # Perform the switch
        x = data.pop(i)
        data.insert(i+1, x)

        # If the switch occurs at a chunk boundary then it will force the
        # extended chunker to create a new file
        # if ((i+1) % chunksize == 0) and i > 0:
        if ((i+1) % chunksize == 0) and (i+1) >= chunksize:
            file_sizes.append((i+1) - sum(file_sizes))

    # Get any last files
    remainder = len(data) - sum(file_sizes)
    if remainder > 0:
        file_sizes.append(remainder)

    # re-insert the header if present
    if has_header is True:
        data.insert(0, header)

    return data, file_sizes


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def check_extended_chunk_files(files, data, data_types, test_key, file_sizes,
                               delimiter, open_method, has_header=True):
    """Check that the chunk files are of the expected structure when compared
    to the source data.

    This is responsible for checking files generated by objects of the
    ``merge_sort.chunks.ExtendedChunks`` class.

    Parameters
    ----------
    files : `list` of `str`
        The chunk files in the order the were created by the chunker. i.e.
        ``chunker.chunk_file_list``.
    data : `list` of `list`
        The source data to compare against.
    data_types : `list` of `str`
        The data types for the columns in ``data``. These are used to convert
        the data that is read in from a chunk file, so it can be compared to
        the source ``data``.
    test_key : `function`
        A sort key function that will be applied to slices of ``data``, should
        be the same as that used in the chunker except for not requiring any
        delimiter split.
    file_sizes : `list` of `int`
        The number of expected data rows in the output files returned by the
        chunker. The length of the list is the number of files.
    delimiter : `str`
        The delimiter used to join the rows prior to adding to the chunker.
    open_method : `function`
        The method used by the chunker to write to chunk files.
    has_header : `bool`, optional, default: `True`
        Were the chunk files written with a header value.
    """
    if has_header is True:
        data.pop(0)

    start_pos = 0
    for f, i in zip(files, file_sizes):
        chunk_file = []
        with open_method(f, 'rt') as infile:
            if has_header is True:
                next(infile)
            for row in infile:
                row = [examples._DTYPE_MAP[d](v)
                       for d, v in zip(data_types, row.split(delimiter))]
                chunk_file.append(row)
        data_chunk = sorted(data[start_pos:start_pos + i], key=test_key)
        start_pos += i
        assert chunk_file == data_chunk, "wrong chunk values"


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# TODO: Test CSV chunkers
@pytest.mark.parametrize(
    ("has_header,ncols,nrows,chunksize,key_dtypes,delimiter,chunk_suffix,"
     "write_method,switches"),
    [
        (True, 10, 10, 1, (str, str), "\t", ".chunk", open, [0]),
        (True, 10, 10, 1, (str, str), "\t", ".chunk", open, [2]),
        (True, 10, 10, 1, (str, str), "\t", ".chunk", open, [2, 5]),
        (True, 10, 10, 1, (str, str), "\t", ".chunk", open, [3, 5]),
        (True, 10, 10, 1, (str, str), "\t", ".chunk", open, [8]),
        (True, 10, 10, 1, (str, str), "\t", ".chunk", open, [0, 3, 5, 7]),
        (True, 10, 10, 3, (str, str), "\t", ".chunk", open, [0]),
        (True, 10, 10, 3, (str, str), "\t", ".chunk", open, [2]),
        (True, 10, 10, 3, (str, str), "\t", ".chunk", open, [2, 5]),
        (True, 10, 10, 3, (str, str), "\t", ".chunk", open, [3, 5]),
        (True, 10, 10, 3, (str, str), "\t", ".chunk", open, [8]),
        (True, 10, 10, 3, (str, str), "\t", ".chunk", open, [0, 3, 5, 6, 7]),
        (True, 10, 10, 5, (str, str), "\t", ".chunk", open, [0]),
        (True, 10, 10, 5, (str, str), "\t", ".chunk", open, [2]),
        (True, 10, 10, 5, (str, str), "\t", ".chunk", open, [2, 5]),
        (True, 10, 10, 5, (str, str), "\t", ".chunk", open, [3, 5]),
        (True, 10, 10, 5, (str, str), "\t", ".chunk", open, [8]),
        (True, 10, 10, 5, (str, str), "\t", ".chunk", open, [0, 3, 5, 6, 7]),
        (True, 10, 10, 20, (str, str), "\t", ".chunk", open, [0]),
        (True, 10, 10, 20, (str, str), "\t", ".chunk", open, [2]),
        (True, 10, 10, 20, (str, str), "\t", ".chunk", open, [2, 5]),
        (True, 10, 10, 20, (str, str), "\t", ".chunk", open, [3, 5]),
        (True, 10, 10, 20, (str, str), "\t", ".chunk", open, [8]),
        (True, 10, 10, 20, (str, str), "\t", ".chunk", open, [0, 3, 5, 6, 7]),
        (True, 10, 10, 1, (str, int), "\t", ".chunk", open, [0]),
        (True, 10, 10, 1, (str, int), "\t", ".chunk", open, [2]),
        (True, 10, 10, 1, (str, int), "\t", ".chunk", open, [2, 5]),
        (True, 10, 10, 1, (str, int), "\t", ".chunk", open, [3, 5]),
        (True, 10, 10, 1, (str, int), "\t", ".chunk", open, [8]),
        (True, 10, 10, 1, (str, int), "\t", ".chunk", open, [0, 3, 5, 7]),
        (True, 10, 10, 3, (str, int), "\t", ".chunk", open, [0]),
        (True, 10, 10, 3, (str, int), "\t", ".chunk", open, [2]),
        (True, 10, 10, 3, (str, int), "\t", ".chunk", open, [2, 5]),
        (True, 10, 10, 3, (str, int), "\t", ".chunk", open, [3, 5]),
        (True, 10, 10, 3, (str, int), "\t", ".chunk", open, [8]),
        (True, 10, 10, 3, (str, int), "\t", ".chunk", open, [0, 3, 5, 6, 7]),
        (True, 10, 10, 5, (str, int), "\t", ".chunk", open, [0]),
        (True, 10, 10, 5, (str, int), "\t", ".chunk", open, [2]),
        (True, 10, 10, 5, (str, int), "\t", ".chunk", open, [2, 5]),
        (True, 10, 10, 5, (str, int), "\t", ".chunk", open, [3, 5]),
        (True, 10, 10, 5, (str, int), "\t", ".chunk", open, [8]),
        (True, 10, 10, 5, (str, int), "\t", ".chunk", open, [0, 3, 5, 6, 7]),
        (True, 10, 10, 20, (str, int), "\t", ".chunk", open, [0]),
        (True, 10, 10, 20, (str, int), "\t", ".chunk", open, [2]),
        (True, 10, 10, 20, (str, int), "\t", ".chunk", open, [2, 5]),
        (True, 10, 10, 20, (str, int), "\t", ".chunk", open, [3, 5]),
        (True, 10, 10, 20, (str, int), "\t", ".chunk", open, [8]),
        (True, 10, 10, 20, (str, int), "\t", ".chunk", open, [0, 3, 5, 6, 7]),
        (True, 10, 10, 1, (str, float), "\t", ".chunk", open, [0]),
        (True, 10, 10, 1, (str, float), "\t", ".chunk", open, [2]),
        (True, 10, 10, 1, (str, float), "\t", ".chunk", open, [2, 5]),
        (True, 10, 10, 1, (str, float), "\t", ".chunk", open, [3, 5]),
        (True, 10, 10, 1, (str, float), "\t", ".chunk", open, [8]),
        (True, 10, 10, 1, (str, float), "\t", ".chunk", open, [0, 3, 5, 7]),
        (True, 10, 10, 3, (str, float), "\t", ".chunk", open, [0]),
        (True, 10, 10, 3, (str, float), "\t", ".chunk", open, [2]),
        (True, 10, 10, 3, (str, float), "\t", ".chunk", open, [2, 5]),
        (True, 10, 10, 3, (str, float), "\t", ".chunk", open, [3, 5]),
        (True, 10, 10, 3, (str, float), "\t", ".chunk", open, [8]),
        (True, 10, 10, 3, (str, float), "\t", ".chunk", open, [0, 3, 5, 6, 7]),
        (True, 10, 10, 5, (str, float), "\t", ".chunk", open, [0]),
        (True, 10, 10, 5, (str, float), "\t", ".chunk", open, [2]),
        (True, 10, 10, 5, (str, float), "\t", ".chunk", open, [2, 5]),
        (True, 10, 10, 5, (str, float), "\t", ".chunk", open, [3, 5]),
        (True, 10, 10, 5, (str, float), "\t", ".chunk", open, [8]),
        (True, 10, 10, 5, (str, float), "\t", ".chunk", open, [0, 3, 5, 6, 7]),
        (True, 10, 10, 20, (str, float), "\t", ".chunk", open, [0]),
        (True, 10, 10, 20, (str, float), "\t", ".chunk", open, [2]),
        (True, 10, 10, 20, (str, float), "\t", ".chunk", open, [2, 5]),
        (True, 10, 10, 20, (str, float), "\t", ".chunk", open, [3, 5]),
        (True, 10, 10, 20, (str, float), "\t", ".chunk", open, [8]),
        (True, 10, 10, 20, (str, float), "\t", ".chunk", open,
         [0, 3, 5, 6, 7]),
        (True, 10, 10, 1, (int, str), "\t", ".chunk", open, [0]),
        (True, 10, 10, 1, (int, str), "\t", ".chunk", open, [2]),
        (True, 10, 10, 1, (int, str), "\t", ".chunk", open, [2, 5]),
        (True, 10, 10, 1, (int, str), "\t", ".chunk", open, [3, 5]),
        (True, 10, 10, 1, (int, str), "\t", ".chunk", open, [8]),
        (True, 10, 10, 1, (int, str), "\t", ".chunk", open, [0, 3, 5, 7]),
        (True, 10, 10, 3, (int, str), "\t", ".chunk", open, [0]),
        (True, 10, 10, 3, (int, str), "\t", ".chunk", open, [2]),
        (True, 10, 10, 3, (int, str), "\t", ".chunk", open, [2, 5]),
        (True, 10, 10, 3, (int, str), "\t", ".chunk", open, [3, 5]),
        (True, 10, 10, 3, (int, str), "\t", ".chunk", open, [8]),
        (True, 10, 10, 3, (int, str), "\t", ".chunk", open, [0, 3, 5, 6, 7]),
        (True, 10, 10, 5, (int, str), "\t", ".chunk", open, [0]),
        (True, 10, 10, 5, (int, str), "\t", ".chunk", open, [2]),
        (True, 10, 10, 5, (int, str), "\t", ".chunk", open, [2, 5]),
        (True, 10, 10, 5, (int, str), "\t", ".chunk", open, [3, 5]),
        (True, 10, 10, 5, (int, str), "\t", ".chunk", open, [8]),
        (True, 10, 10, 5, (int, str), "\t", ".chunk", open, [0, 3, 5, 6, 7]),
        (True, 10, 10, 20, (int, str), "\t", ".chunk", open, [0]),
        (True, 10, 10, 20, (int, str), "\t", ".chunk", open, [2]),
        (True, 10, 10, 20, (int, str), "\t", ".chunk", open, [2, 5]),
        (True, 10, 10, 20, (int, str), "\t", ".chunk", open, [3, 5]),
        (True, 10, 10, 20, (int, str), "\t", ".chunk", open, [8]),
        (True, 10, 10, 20, (int, str), "\t", ".chunk", open, [0, 3, 5, 6, 7]),
        (True, 10, 10, 1, (int, float), "\t", ".chunk", open, [0]),
        (True, 10, 10, 1, (int, float), "\t", ".chunk", open, [2]),
        (True, 10, 10, 1, (int, float), "\t", ".chunk", open, [2, 5]),
        (True, 10, 10, 1, (int, float), "\t", ".chunk", open, [3, 5]),
        (True, 10, 10, 1, (int, float), "\t", ".chunk", open, [8]),
        (True, 10, 10, 1, (int, float), "\t", ".chunk", open, [0, 3, 5, 7]),
        (True, 10, 10, 3, (int, float), "\t", ".chunk", open, [0]),
        (True, 10, 10, 3, (int, float), "\t", ".chunk", open, [2]),
        (True, 10, 10, 3, (int, float), "\t", ".chunk", open, [2, 5]),
        (True, 10, 10, 3, (int, float), "\t", ".chunk", open, [3, 5]),
        (True, 10, 10, 3, (int, float), "\t", ".chunk", open, [8]),
        (True, 10, 10, 3, (int, float), "\t", ".chunk", open, [0, 3, 5, 6, 7]),
        (True, 10, 10, 5, (int, float), "\t", ".chunk", open, [0]),
        (True, 10, 10, 5, (int, float), "\t", ".chunk", open, [2]),
        (True, 10, 10, 5, (int, float), "\t", ".chunk", open, [2, 5]),
        (True, 10, 10, 5, (int, float), "\t", ".chunk", open, [3, 5]),
        (True, 10, 10, 5, (int, float), "\t", ".chunk", open, [8]),
        (True, 10, 10, 5, (int, float), "\t", ".chunk", open, [0, 3, 5, 6, 7]),
        (True, 10, 10, 20, (int, float), "\t", ".chunk", open, [0]),
        (True, 10, 10, 20, (int, float), "\t", ".chunk", open, [2]),
        (True, 10, 10, 20, (int, float), "\t", ".chunk", open, [2, 5]),
        (True, 10, 10, 20, (int, float), "\t", ".chunk", open, [3, 5]),
        (True, 10, 10, 20, (int, float), "\t", ".chunk", open, [8]),
        (True, 10, 10, 20, (int, float), "\t", ".chunk", open,
         [0, 3, 5, 6, 7]),
        (True, 10, 10, 1, (float, str), "\t", ".chunk", open, [0]),
        (True, 10, 10, 1, (float, str), "\t", ".chunk", open, [2]),
        (True, 10, 10, 1, (float, str), "\t", ".chunk", open, [2, 5]),
        (True, 10, 10, 1, (float, str), "\t", ".chunk", open, [3, 5]),
        (True, 10, 10, 1, (float, str), "\t", ".chunk", open, [8]),
        (True, 10, 10, 1, (float, str), "\t", ".chunk", open, [0, 3, 5, 7]),
        (True, 10, 10, 3, (float, str), "\t", ".chunk", open, [0]),
        (True, 10, 10, 3, (float, str), "\t", ".chunk", open, [2]),
        (True, 10, 10, 3, (float, str), "\t", ".chunk", open, [2, 5]),
        (True, 10, 10, 3, (float, str), "\t", ".chunk", open, [3, 5]),
        (True, 10, 10, 3, (float, str), "\t", ".chunk", open, [8]),
        (True, 10, 10, 3, (float, str), "\t", ".chunk", open, [0, 3, 5, 6, 7]),
        (True, 10, 10, 5, (float, str), "\t", ".chunk", open, [0]),
        (True, 10, 10, 5, (float, str), "\t", ".chunk", open, [2]),
        (True, 10, 10, 5, (float, str), "\t", ".chunk", open, [2, 5]),
        (True, 10, 10, 5, (float, str), "\t", ".chunk", open, [3, 5]),
        (True, 10, 10, 5, (float, str), "\t", ".chunk", open, [8]),
        (True, 10, 10, 5, (float, str), "\t", ".chunk", open, [0, 3, 5, 6, 7]),
        (True, 10, 10, 20, (float, str), "\t", ".chunk", open, [0]),
        (True, 10, 10, 20, (float, str), "\t", ".chunk", open, [2]),
        (True, 10, 10, 20, (float, str), "\t", ".chunk", open, [2, 5]),
        (True, 10, 10, 20, (float, str), "\t", ".chunk", open, [3, 5]),
        (True, 10, 10, 20, (float, str), "\t", ".chunk", open, [8]),
        (True, 10, 10, 20, (float, str), "\t", ".chunk", open,
         [0, 3, 5, 6, 7]),
        (True, 10, 10, 1, (float, int), "\t", ".chunk", open, [0]),
        (True, 10, 10, 1, (float, int), "\t", ".chunk", open, [2]),
        (True, 10, 10, 1, (float, int), "\t", ".chunk", open, [2, 5]),
        (True, 10, 10, 1, (float, int), "\t", ".chunk", open, [3, 5]),
        (True, 10, 10, 1, (float, int), "\t", ".chunk", open, [8]),
        (True, 10, 10, 1, (float, int), "\t", ".chunk", open, [0, 3, 5, 7]),
        (True, 10, 10, 3, (float, int), "\t", ".chunk", open, [0]),
        (True, 10, 10, 3, (float, int), "\t", ".chunk", open, [2]),
        (True, 10, 10, 3, (float, int), "\t", ".chunk", open, [2, 5]),
        (True, 10, 10, 3, (float, int), "\t", ".chunk", open, [3, 5]),
        (True, 10, 10, 3, (float, int), "\t", ".chunk", open, [8]),
        (True, 10, 10, 3, (float, int), "\t", ".chunk", open, [0, 3, 5, 6, 7]),
        (True, 10, 10, 5, (float, int), "\t", ".chunk", open, [0]),
        (True, 10, 10, 5, (float, int), "\t", ".chunk", open, [2]),
        (True, 10, 10, 5, (float, int), "\t", ".chunk", open, [2, 5]),
        (True, 10, 10, 5, (float, int), "\t", ".chunk", open, [3, 5]),
        (True, 10, 10, 5, (float, int), "\t", ".chunk", open, [8]),
        (True, 10, 10, 5, (float, int), "\t", ".chunk", open, [0, 3, 5, 6, 7]),
        (True, 10, 10, 20, (float, int), "\t", ".chunk", open, [0]),
        (True, 10, 10, 20, (float, int), "\t", ".chunk", open, [2]),
        (True, 10, 10, 20, (float, int), "\t", ".chunk", open, [2, 5]),
        (True, 10, 10, 20, (float, int), "\t", ".chunk", open, [3, 5]),
        (True, 10, 10, 20, (float, int), "\t", ".chunk", open, [8]),
        (True, 10, 10, 20, (float, int), "\t", ".chunk", open,
         [0, 3, 5, 6, 7]),
        (False, 10, 10, 1, (str, str), "\t", ".chunk", open, [0]),
        (False, 10, 10, 1, (str, str), "\t", ".chunk", open, [2]),
        (False, 10, 10, 1, (str, str), "\t", ".chunk", open, [2, 5]),
        (False, 10, 10, 1, (str, str), "\t", ".chunk", open, [3, 5]),
        (False, 10, 10, 1, (str, str), "\t", ".chunk", open, [8]),
        (False, 10, 10, 1, (str, str), "\t", ".chunk", open, [0, 3, 5, 7]),
        (False, 10, 10, 3, (str, str), "\t", ".chunk", open, [0]),
        (False, 10, 10, 3, (str, str), "\t", ".chunk", open, [2]),
        (False, 10, 10, 3, (str, str), "\t", ".chunk", open, [2, 5]),
        (False, 10, 10, 3, (str, str), "\t", ".chunk", open, [3, 5]),
        (False, 10, 10, 3, (str, str), "\t", ".chunk", open, [8]),
        (False, 10, 10, 3, (str, str), "\t", ".chunk", open, [0, 3, 5, 6, 7]),
        (False, 10, 10, 5, (str, str), "\t", ".chunk", open, [0]),
        (False, 10, 10, 5, (str, str), "\t", ".chunk", open, [2]),
        (False, 10, 10, 5, (str, str), "\t", ".chunk", open, [2, 5]),
        (False, 10, 10, 5, (str, str), "\t", ".chunk", open, [3, 5]),
        (False, 10, 10, 5, (str, str), "\t", ".chunk", open, [8]),
        (False, 10, 10, 5, (str, str), "\t", ".chunk", open, [0, 3, 5, 6, 7]),
        (False, 10, 10, 20, (str, str), "\t", ".chunk", open, [0]),
        (False, 10, 10, 20, (str, str), "\t", ".chunk", open, [2]),
        (False, 10, 10, 20, (str, str), "\t", ".chunk", open, [2, 5]),
        (False, 10, 10, 20, (str, str), "\t", ".chunk", open, [3, 5]),
        (False, 10, 10, 20, (str, str), "\t", ".chunk", open, [8]),
        (False, 10, 10, 20, (str, str), "\t", ".chunk", open, [0, 3, 5, 6, 7]),
    ]
)
def test_extended_chunks(tmpdir, has_header, ncols, nrows, chunksize,
                         key_dtypes, delimiter, chunk_suffix, write_method,
                         switches):
    """Tests for the ``merge_sort.chunks.ExtendedChunks``. This applies the
    chunker to some test data and ensures the expected chunk files are created.
    """
    # Get test data and data types that are in each column
    data, data_types = examples.create_data(has_header=has_header, ncols=ncols,
                                            nrows=nrows)

    # Get the sort key that will be given to the chunker
    sort_key, key_cols = common.get_list_sort_func(data_types, key_dtypes,
                                                   delimiter=delimiter)
    # Get the sort key that will be used on the test data (does not require
    # delimiter splitting in the sort key)
    test_key, key_cols = common.get_list_sort_func(data_types, key_dtypes,
                                                   delimiter=None)

    # This sorts the data and then switches defined pairs of rows
    # this will influence the number of rows in the chunk files
    data, file_sizes = set_switches(
        data, test_key, chunksize, switches, has_header=has_header
    )

    # These conditions must be met for the test to be valid
    assert data == data, "invalid test"

    start_idx = 0
    header = None
    if has_header is True:
        start_idx = 1
        header = delimiter.join(data[0]) + "\n"

    # Initialise the chunker and add the rows
    chunker = chunks.ExtendedChunks(
        tmpdir, key=sort_key, chunksize=chunksize,
        header=header, chunk_suffix=chunk_suffix,
        write_method=write_method
    )

    with chunker as c:
        for row in data[start_idx:]:
            c.add_row(
                "{0}\n".format(delimiter.join([str(i) for i in row]))
            )

    # glob the directory and get the chunk files that have been output
    glob_files = common.get_chunk_files(tmpdir, suffix=chunk_suffix)
    # Make sure we have the expected number of rows
    assert len(glob_files) == len(file_sizes), "wrong file number"

    # Make sure we have output all the files that the chunker thinks it has
    # output
    chunk_files = c.chunk_file_list
    assert sorted(chunk_files) == sorted(glob_files), "wrong files"

    # Now check that all the files have the expected content
    check_extended_chunk_files(chunk_files, data, data_types, test_key,
                               file_sizes, delimiter, write_method,
                               has_header=has_header)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@pytest.mark.parametrize(
    ("has_header,ncols,nrows,chunksize,key_dtypes,delimiter,chunk_suffix,"
     "write_method,use_class"),
    [
        (True, 10, 10, 3, (str, str), "\t", ".chunk", open,
         chunks.CsvSortedChunks),
        (True, 10, 10, 5, (str, str), "\t", ".chunk", open,
         chunks.CsvSortedChunks),
        (True, 10, 10, 20, (str, str), "\t", ".chunk", open,
         chunks.CsvSortedChunks),
        (True, 10, 10, 1, (str, str), "\t", ".chunk", open,
         chunks.CsvSortedChunks),
        (True, 10, 10, 3, (str, int), "\t", ".chunk", open,
         chunks.CsvSortedChunks),
        (True, 10, 10, 5, (str, int), "\t", ".chunk", open,
         chunks.CsvSortedChunks),
        (True, 10, 10, 20, (str, int), "\t", ".chunk", open,
         chunks.CsvSortedChunks),
        (True, 10, 10, 1, (str, int), "\t", ".chunk", open,
         chunks.CsvSortedChunks),
        (True, 10, 10, 3, (str, float), "\t", ".chunk", open,
         chunks.CsvSortedChunks),
        (True, 10, 10, 5, (str, float), "\t", ".chunk", open,
         chunks.CsvSortedChunks),
        (True, 10, 10, 20, (str, float), "\t", ".chunk", open,
         chunks.CsvSortedChunks),
        (True, 10, 10, 1, (str, float), "\t", ".chunk", open,
         chunks.CsvSortedChunks),
        (True, 10, 10, 3, (int, int), "\t", ".chunk", open,
         chunks.CsvSortedChunks),
        (True, 10, 10, 5, (int, int), "\t", ".chunk", open,
         chunks.CsvSortedChunks),
        (True, 10, 10, 20, (int, int), "\t", ".chunk", open,
         chunks.CsvSortedChunks),
        (True, 10, 10, 1, (int, int), "\t", ".chunk", open,
         chunks.CsvSortedChunks),
        (True, 10, 10, 3, (int, float), "\t", ".chunk", open,
         chunks.CsvSortedChunks),
        (True, 10, 10, 5, (int, float), "\t", ".chunk", open,
         chunks.CsvSortedChunks),
        (True, 10, 10, 20, (int, float), "\t", ".chunk", open,
         chunks.CsvSortedChunks),
        (True, 10, 10, 1, (int, float), "\t", ".chunk", open,
         chunks.CsvSortedChunks),
        (True, 10, 10, 3, (float, float), "\t", ".chunk", open,
         chunks.CsvSortedChunks),
        (True, 10, 10, 5, (float, float), "\t", ".chunk", open,
         chunks.CsvSortedChunks),
        (True, 10, 10, 20, (float, float), "\t", ".chunk", open,
         chunks.CsvSortedChunks),
        (True, 10, 10, 1, (float, float), "\t", ".chunk", open,
         chunks.CsvSortedChunks),
        (True, 10, 10, 3, (str, str), "\t", ".chunk", open,
         chunks.CsvSortedListChunks),
        (True, 10, 10, 5, (str, str), "\t", ".chunk", open,
         chunks.CsvSortedListChunks),
        (True, 10, 10, 20, (str, str), "\t", ".chunk", open,
         chunks.CsvSortedListChunks),
        (True, 10, 10, 1, (str, str), "\t", ".chunk", open,
         chunks.CsvSortedListChunks),
        (True, 10, 10, 3, (str, int), "\t", ".chunk", open,
         chunks.CsvSortedListChunks),
        (True, 10, 10, 5, (str, int), "\t", ".chunk", open,
         chunks.CsvSortedListChunks),
        (True, 10, 10, 20, (str, int), "\t", ".chunk", open,
         chunks.CsvSortedListChunks),
        (True, 10, 10, 1, (str, int), "\t", ".chunk", open,
         chunks.CsvSortedListChunks),
        (True, 10, 10, 3, (str, float), "\t", ".chunk", open,
         chunks.CsvSortedListChunks),
        (True, 10, 10, 5, (str, float), "\t", ".chunk", open,
         chunks.CsvSortedListChunks),
        (True, 10, 10, 20, (str, float), "\t", ".chunk", open,
         chunks.CsvSortedListChunks),
        (True, 10, 10, 1, (str, float), "\t", ".chunk", open,
         chunks.CsvSortedListChunks),
        (True, 10, 10, 3, (int, int), "\t", ".chunk", open,
         chunks.CsvSortedListChunks),
        (True, 10, 10, 5, (int, int), "\t", ".chunk", open,
         chunks.CsvSortedListChunks),
        (True, 10, 10, 20, (int, int), "\t", ".chunk", open,
         chunks.CsvSortedListChunks),
        (True, 10, 10, 1, (int, int), "\t", ".chunk", open,
         chunks.CsvSortedListChunks),
        (True, 10, 10, 3, (int, float), "\t", ".chunk", open,
         chunks.CsvSortedListChunks),
        (True, 10, 10, 5, (int, float), "\t", ".chunk", open,
         chunks.CsvSortedListChunks),
        (True, 10, 10, 20, (int, float), "\t", ".chunk", open,
         chunks.CsvSortedListChunks),
        (True, 10, 10, 1, (int, float), "\t", ".chunk", open,
         chunks.CsvSortedListChunks),
        (True, 10, 10, 3, (float, float), "\t", ".chunk", open,
         chunks.CsvSortedListChunks),
        (True, 10, 10, 5, (float, float), "\t", ".chunk", open,
         chunks.CsvSortedListChunks),
        (True, 10, 10, 20, (float, float), "\t", ".chunk", open,
         chunks.CsvSortedListChunks),
        (True, 10, 10, 1, (float, float), "\t", ".chunk", open,
         chunks.CsvSortedListChunks),
        (False, 10, 10, 3, (str, str), "\t", ".chunk", open,
         chunks.CsvSortedChunks),
        (False, 10, 10, 5, (str, str), "\t", ".chunk", open,
         chunks.CsvSortedChunks),
        (False, 10, 10, 20, (str, str), "\t", ".chunk", open,
         chunks.CsvSortedChunks),
        (False, 10, 10, 1, (str, str), "\t", ".chunk", open,
         chunks.CsvSortedChunks),
    ]
)
def test_csv_sorted_chunks(tmpdir, has_header, ncols, nrows, chunksize,
                           key_dtypes, delimiter, chunk_suffix, write_method,
                           use_class):
    """Tests for the ``merge_sort.chunks.CsvSortedChunks`` and
    ``merge_sort.chunks.CsvSortedListChunks``. This applies the chunkers to
    some test data and ensures the expected chunk files are created.
    """
    # Get test data and data types that are in each column
    data, data_types = examples.create_data(has_header=has_header, ncols=ncols,
                                            nrows=nrows)
    # Get the sort key that will be used on the data (does not require
    # delimiter splitting in the sort key)
    sort_key, key_cols = common.get_list_sort_func(data_types, key_dtypes,
                                                   delimiter=None)

    # These conditions must be met for the test to be valid
    assert data == data, "invalid test"

    start_idx = 0
    header = None
    if has_header is True:
        start_idx = 1
        header = data[0]

    # Initialise the chunker and add the rows
    chunker = use_class(
        tmpdir, key=sort_key, chunksize=chunksize,
        header=header, chunk_suffix=chunk_suffix,
        write_method=write_method, delimiter=delimiter,
        lineterminator=os.linesep
    )

    with chunker as c:
        for row in data[start_idx:]:
            c.add_row(row)

    # glob the directory and get the chunk files that have been output
    glob_files = common.get_chunk_files(tmpdir, suffix=chunk_suffix)

    # Make sure we have the expected number of rows
    assert len(glob_files) == math.ceil(
        (len(data) - int(has_header)) / chunksize
    ), "wrong file number"

    # Make sure we have output all the files that the chunker thinks it has
    # output
    chunk_files = c.chunk_file_list
    assert sorted(chunk_files) == sorted(glob_files), "wrong files"

    # Now check that all the files have the expected content
    check_chunk_files(chunk_files, data, data_types, sort_key, chunksize,
                      delimiter, write_method, has_header=has_header,
                      lineterminator=os.linesep)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@pytest.mark.parametrize(
    ("ncols,nrows,chunksize,key_dtypes,delimiter,chunk_suffix,"
     "write_method,use_class"),
    [
        (10, 10, 3, (str, str), "\t", ".chunk", open,
         chunks.CsvDictSortedChunks),
        (10, 10, 5, (str, str), "\t", ".chunk", open,
         chunks.CsvDictSortedChunks),
        (10, 10, 20, (str, str), "\t", ".chunk", open,
         chunks.CsvDictSortedChunks),
        (10, 10, 1, (str, str), "\t", ".chunk", open,
         chunks.CsvDictSortedChunks),
        (10, 10, 3, (str, int), "\t", ".chunk", open,
         chunks.CsvDictSortedChunks),
        (10, 10, 5, (str, int), "\t", ".chunk", open,
         chunks.CsvDictSortedChunks),
        (10, 10, 20, (str, int), "\t", ".chunk", open,
         chunks.CsvDictSortedChunks),
        (10, 10, 1, (str, int), "\t", ".chunk", open,
         chunks.CsvDictSortedChunks),
        (10, 10, 3, (str, float), "\t", ".chunk", open,
         chunks.CsvDictSortedChunks),
        (10, 10, 5, (str, float), "\t", ".chunk", open,
         chunks.CsvDictSortedChunks),
        (10, 10, 20, (str, float), "\t", ".chunk", open,
         chunks.CsvDictSortedChunks),
        (10, 10, 1, (str, float), "\t", ".chunk", open,
         chunks.CsvDictSortedChunks),
        (10, 10, 3, (int, int), "\t", ".chunk", open,
         chunks.CsvDictSortedChunks),
        (10, 10, 5, (int, int), "\t", ".chunk", open,
         chunks.CsvDictSortedChunks),
        (10, 10, 20, (int, int), "\t", ".chunk", open,
         chunks.CsvDictSortedChunks),
        (10, 10, 1, (int, int), "\t", ".chunk", open,
         chunks.CsvDictSortedChunks),
        (10, 10, 3, (int, float), "\t", ".chunk", open,
         chunks.CsvDictSortedChunks),
        (10, 10, 5, (int, float), "\t", ".chunk", open,
         chunks.CsvDictSortedChunks),
        (10, 10, 20, (int, float), "\t", ".chunk", open,
         chunks.CsvDictSortedChunks),
        (10, 10, 1, (int, float), "\t", ".chunk", open,
         chunks.CsvDictSortedChunks),
        (10, 10, 3, (float, float), "\t", ".chunk", open,
         chunks.CsvDictSortedChunks),
        (10, 10, 5, (float, float), "\t", ".chunk", open,
         chunks.CsvDictSortedChunks),
        (10, 10, 20, (float, float), "\t", ".chunk", open,
         chunks.CsvDictSortedChunks),
        (10, 10, 1, (float, float), "\t", ".chunk", open,
         chunks.CsvDictSortedChunks),
        (10, 10, 3, (str, str), "\t", ".chunk", open,
         chunks.CsvDictSortedListChunks),
        (10, 10, 5, (str, str), "\t", ".chunk", open,
         chunks.CsvDictSortedListChunks),
        (10, 10, 20, (str, str), "\t", ".chunk", open,
         chunks.CsvDictSortedListChunks),
        (10, 10, 1, (str, str), "\t", ".chunk", open,
         chunks.CsvDictSortedListChunks),
        (10, 10, 3, (str, int), "\t", ".chunk", open,
         chunks.CsvDictSortedListChunks),
        (10, 10, 5, (str, int), "\t", ".chunk", open,
         chunks.CsvDictSortedListChunks),
        (10, 10, 20, (str, int), "\t", ".chunk", open,
         chunks.CsvDictSortedListChunks),
        (10, 10, 1, (str, int), "\t", ".chunk", open,
         chunks.CsvDictSortedListChunks),
        (10, 10, 3, (str, float), "\t", ".chunk", open,
         chunks.CsvDictSortedListChunks),
        (10, 10, 5, (str, float), "\t", ".chunk", open,
         chunks.CsvDictSortedListChunks),
        (10, 10, 20, (str, float), "\t", ".chunk", open,
         chunks.CsvDictSortedListChunks),
        (10, 10, 1, (str, float), "\t", ".chunk", open,
         chunks.CsvDictSortedListChunks),
        (10, 10, 3, (int, int), "\t", ".chunk", open,
         chunks.CsvDictSortedListChunks),
        (10, 10, 5, (int, int), "\t", ".chunk", open,
         chunks.CsvDictSortedListChunks),
        (10, 10, 20, (int, int), "\t", ".chunk", open,
         chunks.CsvDictSortedListChunks),
        (10, 10, 1, (int, int), "\t", ".chunk", open,
         chunks.CsvDictSortedListChunks),
        (10, 10, 3, (int, float), "\t", ".chunk", open,
         chunks.CsvDictSortedListChunks),
        (10, 10, 5, (int, float), "\t", ".chunk", open,
         chunks.CsvDictSortedListChunks),
        (10, 10, 20, (int, float), "\t", ".chunk", open,
         chunks.CsvDictSortedListChunks),
        (10, 10, 1, (int, float), "\t", ".chunk", open,
         chunks.CsvDictSortedListChunks),
        (10, 10, 3, (float, float), "\t", ".chunk", open,
         chunks.CsvDictSortedListChunks),
        (10, 10, 5, (float, float), "\t", ".chunk", open,
         chunks.CsvDictSortedListChunks),
        (10, 10, 20, (float, float), "\t", ".chunk", open,
         chunks.CsvDictSortedListChunks),
        (10, 10, 1, (float, float), "\t", ".chunk", open,
         chunks.CsvDictSortedListChunks),
    ]
)
def test_csv_dict_sorted_chunks(tmpdir, ncols, nrows, chunksize, key_dtypes,
                                delimiter, chunk_suffix, write_method,
                                use_class):
    """Tests for the ``merge_sort.chunks.CsvDictSortedChunks`` and
    ``merge_sort.chunks.CsvDictSortedListChunks``. This applies the chunkers to
    some test data and ensures the expected chunk files are created.
    """
    # Get test data and data types that are in each column
    data, data_types = examples.create_data(
        has_header=True, ncols=ncols, nrows=nrows, use_dict=True
    )

    # Get the sort key that will be used on the data (does not require
    # delimiter splitting in the sort key)
    sort_key, key_cols = common.get_dict_sort_func(data_types, key_dtypes)

    # These conditions must be met for the test to be valid
    assert data == data, "invalid test"

    # Initialise the chunker and add the rows
    chunker = use_class(
        tmpdir, key=sort_key, chunksize=chunksize,
        header=data[0], chunk_suffix=chunk_suffix,
        write_method=write_method, delimiter=delimiter,
        lineterminator=os.linesep
    )

    with chunker as c:
        for row in data[1:]:
            c.add_row(row)

    # glob the directory and get the chunk files that have been output
    glob_files = common.get_chunk_files(tmpdir, suffix=chunk_suffix)

    # Make sure we have the expected number of rows
    assert len(glob_files) == math.ceil((len(data) - 1) / chunksize), \
        "wrong file number"

    # Make sure we have output all the files that the chunker thinks it has
    # output
    chunk_files = c.chunk_file_list
    assert sorted(chunk_files) == sorted(glob_files), "wrong files"

    # Now check that all the files have the expected content
    check_chunk_files(chunk_files, data, data_types, sort_key, chunksize,
                      delimiter, write_method, has_header=True,
                      lineterminator=os.linesep)
