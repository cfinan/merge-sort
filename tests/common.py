"""Tests for chunking data rows into sorted chunks using different methods.
"""
from merge_sort import examples
import glob
import os
# import pprint as pp


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_list_sort_func(col_data_types, key_data_types, delimiter=None):
    """Get a source function based on some requested key data types.

    Parameters
    ----------
    col_data_types : `list` of `str`
        The data types for the columns of the data set. The strings should be
        either ``'str'``, ``'float'`` or ``'int'``.
    key_data_types : `list` or `tuple` of `type`
        The requested data types, either ``int``, `str`` or ``float``. These
        should be types not strings as in ``col_data_types``.
    delimiter : `str`, optional, default: `NoneType`
        The delimiter to use for a split in the sort function. If not provided
        then no split will occur in the sort function.

    Returns
    -------
    sort_function : `function`
        A function to use in the sorting test.
    key_cols : `tuple` of `(type, column_number (int))`
        The key columns that were enclosed in the function.
    """
    seen_dt = set()
    last_idx = 0
    key_cols = []
    for i in key_data_types:
        start_idx = 0
        if i in seen_dt:
            start_idx = last_idx
        idx = col_data_types[start_idx:].index(i.__name__) + last_idx
        key_cols.append((examples._DTYPE_MAP[i.__name__], idx))
        last_idx = idx + 1
        seen_dt.add(i)

    def _sort_func(x):
        if delimiter is not None:
            x = x.split(delimiter)
        return tuple([i(x[j]) for i, j in key_cols])

    return _sort_func, key_cols


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_dict_sort_func(col_data_types, key_data_types):
    """Get a sort function based on some requested key data types. This gets a
    function that can be used on dictionaries.

    Parameters
    ----------
    col_data_types : `dict` of `str`
        The data types for the columns of the data set. The keys are the column
        names and the values are strings names of data types either ``'str'``,
        ``'float'`` or ``'int'``.
    key_data_types : `list` or `tuple` of `type`
        The requested data types, either ``int``, `str`` or ``float``. These
        should be types not strings as in ``col_data_types``.

    Returns
    -------
    sort_function : `function`
        A function to use in the sorting test.
    key_cols : `tuple` of `(type, column_number (int))`
        The key columns that were enclosed in the function.
    """
    seen_dt = set()
    last_idx = 0
    key_cols = []

    collookup = list(col_data_types.keys())
    dtlookup = list(col_data_types.values())

    for i in key_data_types:
        start_idx = 0
        if i in seen_dt:
            start_idx = last_idx
        idx = dtlookup[start_idx:].index(i.__name__) + last_idx
        key_cols.append((examples._DTYPE_MAP[i.__name__], collookup[idx]))
        last_idx = idx + 1
        seen_dt.add(i)

    def _sort_func(x):
        return tuple([i(x[j]) for i, j in key_cols])

    return _sort_func, key_cols


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_chunk_files(dir, prefix=None, suffix=None):
    """Get all the chunk files that have been written by the chunker.

    Parameters
    ----------
    dir : `str`
        The directory that should contain the chunk files.
    prefix : `str`, optional, default: `NoneType`
        Any prefix that the chunk files have been given.
    suffix : `str`, optional, default: `NoneType`
        Any suffix that the chunk files have been given.

    Returns
    -------
    chunk_files : `list` of `str`
        Any chunk files that have been found, if none are found then this
        will be empty.
    """
    prefix = prefix or ""
    suffix = suffix or ""
    return glob.glob(os.path.join(dir, f"{prefix}*{suffix}"))
